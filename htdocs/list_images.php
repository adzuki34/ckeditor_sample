<?php
echo <<<EOD
<script>
function getreturn(tmpstr, num)
{
    window.opener.CKEDITOR.tools.callFunction(num, '/show_image.php?filename='+tmpstr);
    window.close();
}
</script>
EOD;

$num = $_GET['CKEditorFuncNum'];
$image_dir = "/tmp";
if ($handle = opendir($image_dir)) {
	echo "画像一覧<br><br>";
	while (false !== ($file = readdir($handle))) {
		if (preg_match("/\.(jpg|jpeg|gif|png)$/", $file)) {
			echo '<p><a href="#" onclick="getreturn(\'' . $file . '\', \'' . $num . '\')"><img border="0" width="100" src="show_image.php?filename=' . $file. '"></a></p>';
		}
	}
	closedir($handle);
}
