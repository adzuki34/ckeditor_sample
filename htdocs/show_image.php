<?php

$filename = $_GET["filename"];
$base_dir = "/tmp";

$image_path = "$base_dir/$filename";

if (file_exists($image_path)) {
	$finfo = new finfo(FILEINFO_MIME);
	$mime = $finfo->file($image_path);
	header('Content-Type: ' . $mime);
	header('Content-Length: ' . filesize($image_path));
	readfile($image_path);
}
