<?php
$ckeditor_func_num = $_GET['CKEditorFuncNum'];

$upload_dir = '/tmp';
$tmp_name = $_FILES["upload"]["tmp_name"];
$name = $_FILES["upload"]["name"];
$image_path = "$upload_dir/$name";

if (move_uploaded_file($tmp_name, $image_path)) {
	$script = "<script>window.parent.CKEDITOR.tools.callFunction(" .
		$ckeditor_func_num . ", '" . $image_path . ", '');</script>";
} else {
	exit;
}

echo $script;
