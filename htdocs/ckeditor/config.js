/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	//config.width = '1200px';
	//config.height = '500px';
	//config.resize_enabled = true;

	// ツールバー
	config.toolbar = [
		['Source','Save','NewPage','Preview','Templates']
		,['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print','SpellChecker']
		,['Undo','Redo','Find','Replace','SelectAll','RemoveFormat']
		,['Form','Checkbox','Radio','TextField','Textarea','Select','Button','ImageButton','HiddenField']
		,['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
		,['Bold','Italic','Underline','Strike','Subscript','Superscript']
		,['NumberedList','BulletedList','Outdent','Indent','Blockquote']
		,['Link','Unlink','Anchor']
		,['Image','Flash','Table','HorizontalRule',/*'Smiley','SpecialChar','PageBreak'*/]
		,['Styles','Format','Font','FontSize','TextColor','BGColor']
		,['ShowBlocks']
	];

	// 画像アップロード
	config.filebrowserUploadUrl = 'save_image.php';
	config.filebrowserBrowseUrl = 'list_images.php';
	config.filebrowserWindowWidth = '640';
	config.filebrowserWindowHeight = '480';
};
